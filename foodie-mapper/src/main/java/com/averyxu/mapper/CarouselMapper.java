package com.averyxu.mapper;

import com.averyxu.pojo.Carousel;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface CarouselMapper {
    List<Carousel> queryAll(int isShow);
}
