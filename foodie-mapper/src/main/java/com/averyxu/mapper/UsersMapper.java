package com.averyxu.mapper;

import com.averyxu.pojo.Users;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface UsersMapper {

    Users queryUsersByUsername(String username);

    void createUser(Users user);

    Users queryUserForLogin(String username, String password);

}
