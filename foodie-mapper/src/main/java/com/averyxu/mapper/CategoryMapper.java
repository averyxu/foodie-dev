package com.averyxu.mapper;

import com.averyxu.pojo.Category;
import com.averyxu.pojo.vo.CatsVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Mapper
@Repository
public interface CategoryMapper {

    List<Category> queryAllRootLevelCats();

    List<CatsVO> getSubCats(int rootCatsId);

    List getSixNewItemsLazy(@Param("paramsMap") Map<String, Object> map);
}
