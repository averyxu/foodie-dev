package com.averyxu.mapper;

import com.averyxu.pojo.Stu;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface StuMapper {

    List<Stu> queryAll();
    Stu getStuInfo(int id);
    void saveStu(Stu stu);
    void updateStu(Stu stu);
    void deleteStu(int id);

}
