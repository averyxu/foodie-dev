package com.averyxu.mapper;

import com.averyxu.pojo.Items;
import com.averyxu.pojo.ItemsImg;
import com.averyxu.pojo.ItemsParam;
import com.averyxu.pojo.ItemsSpec;
import com.averyxu.pojo.vo.ItemCommentVO;
import com.averyxu.pojo.vo.SearchItemsVO;
import com.averyxu.pojo.vo.ShopcartVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Mapper
@Repository
public interface ItemsMapper {

    Items queryItemById(String itemId);

    List<ItemsImg> queryItemImgList(String itemId);

    List<ItemsSpec> queryItemSpecList(String itemId);

    ItemsParam queryItemParamById(String itemId);

    Integer queryCommentCounts(String itemId, Integer level);

    List<ItemCommentVO> queryItemComments(@Param("paramsMap") Map<String, Object> map);

    List<SearchItemsVO> searchItems(@Param("paramsMap") Map<String, Object> map);

    List<SearchItemsVO> searchItemsByThirdCat(@Param("paramsMap") Map<String, Object> map);

    List<ShopcartVO> queryItemsBySpecIds(@Param("paramsList") List specIdsList);
}
