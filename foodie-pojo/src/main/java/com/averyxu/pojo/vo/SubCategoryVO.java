package com.averyxu.pojo.vo;

public class SubCategoryVO {
    // c.id as subId, c.name as subName, c.`type` as subType, c.father_id as subFatherId
    private int subId;
    private String subName;
    private String subType;
    private int subFatherId;

    public int getSubId() {
        return subId;
    }

    public void setSubId(int subId) {
        this.subId = subId;
    }

    public String getSubName() {
        return subName;
    }

    public void setSubName(String subName) {
        this.subName = subName;
    }

    public String getSubType() {
        return subType;
    }

    public void setSubType(String subType) {
        this.subType = subType;
    }

    public int getSubFatherId() {
        return subFatherId;
    }

    public void setSubFatherId(int subFatherId) {
        this.subFatherId = subFatherId;
    }
}
