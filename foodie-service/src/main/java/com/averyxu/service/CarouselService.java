package com.averyxu.service;

import com.averyxu.pojo.Carousel;

import java.util.List;

public interface CarouselService {

    List<Carousel> queryAll(int isShow);
}
