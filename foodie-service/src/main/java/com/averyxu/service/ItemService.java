package com.averyxu.service;

import com.averyxu.pojo.Items;
import com.averyxu.pojo.ItemsImg;
import com.averyxu.pojo.ItemsParam;
import com.averyxu.pojo.ItemsSpec;
import com.averyxu.pojo.vo.CommentLevelCountsVO;
import com.averyxu.pojo.vo.ShopcartVO;
import com.averyxu.utils.PagedGridResult;

import java.util.List;

public interface ItemService {

    Items queryItemById(String itemId);

    List<ItemsImg> queryItemImgList(String itemId);

    List<ItemsSpec> queryItemSpecList(String itemId);

    ItemsParam queryItemParamById(String itemId);

    CommentLevelCountsVO queryCommentCounts(String itemId);

    PagedGridResult queryPagedComments(String itemId, Integer level, Integer page, Integer pageSize);

    PagedGridResult searchItems(String keywords, String sort, Integer page, Integer pageSize);

    PagedGridResult searchItemsByThirdCat(Integer catId, String sort, Integer page, Integer pageSize);

    List<ShopcartVO> queryItemsBySpecIds(String specIds);
}
