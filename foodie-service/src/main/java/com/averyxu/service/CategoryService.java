package com.averyxu.service;

import com.averyxu.pojo.Category;
import com.averyxu.pojo.vo.CatsVO;
import com.averyxu.pojo.vo.NewItemsVO;

import java.util.List;

public interface CategoryService {

    List<Category> queryAllRootLevelCats();

    List<CatsVO> getSubCats(int rootCatsId);

    List<NewItemsVO> getSixNewItemsLazy(int rootCatId);
}
