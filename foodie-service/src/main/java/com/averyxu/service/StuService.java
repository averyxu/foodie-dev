package com.averyxu.service;

import com.averyxu.pojo.Stu;

import java.util.List;

public interface StuService {
    List<Stu> queryAll();
    Stu getStuInfo(int id);
    void saveStu();
    void updateStu(int id);
    void deleteStu(int id);

    void saveParent();
    void saveChildren();
}
