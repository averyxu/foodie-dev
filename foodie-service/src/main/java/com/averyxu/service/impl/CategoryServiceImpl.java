package com.averyxu.service.impl;

import com.averyxu.mapper.CategoryMapper;
import com.averyxu.pojo.Category;
import com.averyxu.pojo.vo.CatsVO;
import com.averyxu.pojo.vo.NewItemsVO;
import com.averyxu.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class CategoryServiceImpl implements CategoryService {

    @Autowired
    private CategoryMapper categoryMapper;

    @Transactional(propagation = Propagation.SUPPORTS)
    @Override
    public List<Category> queryAllRootLevelCats() {
        return categoryMapper.queryAllRootLevelCats();
    }

    @Transactional(propagation = Propagation.SUPPORTS)
    @Override
    public List<CatsVO> getSubCats(int rootCatsId) {
        return categoryMapper.getSubCats(rootCatsId);
    }

    @Transactional(propagation = Propagation.SUPPORTS)
    @Override
    public List<NewItemsVO> getSixNewItemsLazy(int rootCatId) {
        Map<String, Object> map = new HashMap<>();
        map.put("rootCatId", rootCatId);
        return categoryMapper.getSixNewItemsLazy(map);
    }
}
