package com.averyxu.service.impl;

import com.averyxu.mapper.StuMapper;
import com.averyxu.pojo.Stu;
import com.averyxu.service.StuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class StuServiceImpl implements StuService {
    @Autowired
    private StuMapper stuMapper;

    @Override
    public List<Stu> queryAll() {
        return stuMapper.queryAll();
    }

    @Override
    public Stu getStuInfo(int id) {
        return stuMapper.getStuInfo(id);
    }

    @Transactional(propagation = Propagation.REQUIRED)
    @Override
    public void saveStu() {
        Stu stu = new Stu();
        stu.setName("Jack");
        stu.setAge(19);
        stuMapper.saveStu(stu);
    }

    @Transactional(propagation = Propagation.REQUIRED)
    @Override
    public void updateStu(int id) {
        Stu stu = new Stu();
        stu.setName("Tom");
        stu.setAge(18);
        stu.setId(id);
        stuMapper.updateStu(stu);
    }

    @Transactional(propagation = Propagation.REQUIRED)
    @Override
    public void deleteStu(int id) {
        stuMapper.deleteStu(id);
    }


    public void saveParent() {
        Stu stu = new Stu();
        stu.setName("parent");
        stu.setAge(19);
        stuMapper.saveStu(stu);
    }
    @Transactional(propagation = Propagation.NESTED)
    public void saveChildren() {
        saveChild1();
        //int a = 1 / 0;
        saveChild2();
    }

    public void saveChild1() {
        Stu stu1 = new Stu();
        stu1.setName("child-1");
        stu1.setAge(11);
        stuMapper.saveStu(stu1);
    }
    public void saveChild2() {
        Stu stu2 = new Stu();
        stu2.setName("child-2");
        stu2.setAge(22);
        stuMapper.saveStu(stu2);
    }

}
