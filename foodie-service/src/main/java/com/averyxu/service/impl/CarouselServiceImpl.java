package com.averyxu.service.impl;

import com.averyxu.mapper.CarouselMapper;
import com.averyxu.pojo.Carousel;
import com.averyxu.service.CarouselService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CarouselServiceImpl implements CarouselService {

    @Autowired
    private CarouselMapper carouselMapper;

    @Override
    public List<Carousel> queryAll(int isShow) {
        return carouselMapper.queryAll(isShow);
    }
}
