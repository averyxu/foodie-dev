package com.averyxu.service.impl;

import com.averyxu.service.StuService;
import com.averyxu.service.TestTransService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
public class TestTransServiceImpl implements TestTransService {

    @Autowired
    private StuService stuService;

    @Transactional(propagation = Propagation.REQUIRED)
    @Override
    public void testPropagationTrans() {
        stuService.saveParent();
        try {
            stuService.saveChildren();
        }catch (Exception e) {
            e.printStackTrace();
        }
//        int a = 1 / 0;
    }
}
