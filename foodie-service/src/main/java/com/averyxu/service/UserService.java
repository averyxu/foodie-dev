package com.averyxu.service;

import com.averyxu.pojo.Users;
import com.averyxu.pojo.bo.UserBO;

public interface UserService {

    boolean queryUsernameIsExist(String username);

    Users createUser(UserBO userBO);

    Users queryUserForLogin(String username, String password);
}
