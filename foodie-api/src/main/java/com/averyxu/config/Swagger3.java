package com.averyxu.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

@Configuration
public class Swagger3 {

    @Bean
    public Docket docket() {
        return new Docket(DocumentationType.OAS_30).apiInfo(
                new ApiInfoBuilder()
                        .contact(new Contact("Averyxu", "", "Averyxu@qq.com"))
                        .title("天天吃货 电商平台接口API")
                        .description("专为天天吃货提供的api文档")
                        .version("1.0.1")
                        .build()
        );
    }
}
