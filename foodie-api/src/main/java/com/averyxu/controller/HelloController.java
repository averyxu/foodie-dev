package com.averyxu.controller;

import com.averyxu.pojo.Stu;
import com.averyxu.service.StuService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@ApiIgnore
@RestController
public class HelloController {
    private final static Logger log = LoggerFactory.getLogger(HelloController.class);

    @Autowired
    private StuService stuService;


    @GetMapping("/hello")
    public Object hello() {
        log.debug("debug:hello==================");
        log.info("info:hello==================");
        log.warn("warn:hello==================");
        log.error("error:hello==================");
        return "hello";
    }

    @Transactional(propagation = Propagation.SUPPORTS)
    @GetMapping("/getStu")
    public Object queryAll() {
        return stuService.queryAll();
    }

    @Transactional(propagation = Propagation.SUPPORTS)
    @GetMapping(value = "/findStu/{id}")  // http://localhost:8088/findStu/1211
    public Stu findStu(@PathVariable(name = "id") int id) {
        return stuService.getStuInfo(id);
    }

    @Transactional(propagation = Propagation.SUPPORTS)
    @GetMapping(value = "/findStuById")  // http://localhost:8088/findStuById?id=1211
    public Stu findStuById(int id) {
        return stuService.getStuInfo(id);
    }

    @PostMapping("/saveStu")
    public String saveStu() {
        stuService.saveStu();
        return "OK";
    }

    @PostMapping("/updateStu")
    public String updateStu(int id) {
        stuService.updateStu(id);
        return "OK";
    }

    @PostMapping("/deleteStu")
    public String deleteStu(int id) {
        stuService.deleteStu(id);
        return "OK";
    }

    @GetMapping("/setSession")
    public Object setSession(HttpServletRequest request) {
        HttpSession session =  request.getSession();
        session.setAttribute("userInfo", "new user");
        session.setMaxInactiveInterval(3600);
        //session.getAttribute("userInfo");
        //session.removeAttribute("userInfo");
        return "OK";
    }
}
