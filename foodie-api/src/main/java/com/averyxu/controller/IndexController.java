package com.averyxu.controller;

import com.averyxu.enums.YesOrNo;
import com.averyxu.pojo.Carousel;
import com.averyxu.pojo.Category;
import com.averyxu.pojo.vo.CatsVO;
import com.averyxu.pojo.vo.NewItemsVO;
import com.averyxu.service.CarouselService;
import com.averyxu.service.CategoryService;
import com.averyxu.utils.JSONResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Api(value = "首页", tags = {"首页显示的相关接口"})
@RestController
@RequestMapping("index")
public class IndexController {

    @Autowired
    private CarouselService carouselService;
    @Autowired
    private CategoryService categoryService;


    @ApiOperation(value = "获取首页轮播图列表", notes = "获取首页轮播图列表" ,httpMethod = "GET")
    @Transactional(propagation = Propagation.SUPPORTS)
    @GetMapping("/carousel")
    public JSONResult queryAllCarousel() {
        List<Carousel> list = carouselService.queryAll(YesOrNo.YES.type);
        return JSONResult.ok(list);
    }

    @ApiOperation(value = "获取商品分类 一级分类", notes = "获取商品分类 一级分类" ,httpMethod = "GET")
    @Transactional(propagation = Propagation.SUPPORTS)
    @GetMapping("/cats")
    public JSONResult queryAllCats() {
        List<Category> list = categoryService.queryAllRootLevelCats();
        return JSONResult.ok(list);
    }

    @ApiOperation(value = "获取商品分类 子级分类", notes = "获取商品分类 子级分类" ,httpMethod = "GET")
    @Transactional(propagation = Propagation.SUPPORTS)
    @GetMapping("/subCat/{rootCatId}")
    public JSONResult subCats(@ApiParam(name = "rootCatId", value = "路径参数 一级分类ID", required = true) @PathVariable Integer rootCatId) {
        if (rootCatId == null) {
            return JSONResult.errorMsg("分类不存在");
        }
        List<CatsVO> list = categoryService.getSubCats(rootCatId);
        return JSONResult.ok(list);
    }

    @ApiOperation(value = "查询每个一级分类下的最新6条记录", notes = "查询每个一级分类下的最新6条记录" ,httpMethod = "GET")
    @Transactional(propagation = Propagation.SUPPORTS)
    @GetMapping("/sixNewItems/{rootCatId}")
    public JSONResult sixNewItems(@ApiParam(name = "rootCatId", value = "路径参数 一级分类ID", required = true) @PathVariable Integer rootCatId) {
        if (rootCatId == null) {
            return JSONResult.errorMsg("分类不存在");
        }
        List<NewItemsVO> list = categoryService.getSixNewItemsLazy(rootCatId);
        return JSONResult.ok(list);
    }

}
