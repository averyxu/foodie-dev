package com.averyxu.controller;

import com.averyxu.pojo.Items;
import com.averyxu.pojo.ItemsImg;
import com.averyxu.pojo.ItemsParam;
import com.averyxu.pojo.ItemsSpec;
import com.averyxu.pojo.vo.CommentLevelCountsVO;
import com.averyxu.pojo.vo.ItemInfoVO;
import com.averyxu.pojo.vo.ShopcartVO;
import com.averyxu.service.ItemService;
import com.averyxu.utils.JSONResult;
import com.averyxu.utils.PagedGridResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Api(value = "商品接口", tags = {"商品信息显示的相关接口"})
@RestController
@RequestMapping("items")
public class ItemsController extends BaseController{

    @Autowired
    private ItemService itemService;

    @ApiOperation(value = "查询商品详情", notes = "查询商品详情" ,httpMethod = "GET")
    @Transactional(propagation = Propagation.SUPPORTS)
    @GetMapping("/info/{itemId}")
    public JSONResult itemInfo(@ApiParam(name = "itemId", value = "商品id", required = true)
                                           @PathVariable String itemId) {
        if (StringUtils.isBlank(itemId)) {
            return JSONResult.errorMsg("商品ID不能位空");
        }

        Items items = itemService.queryItemById(itemId);
        List<ItemsImg> itemsImgs = itemService.queryItemImgList(itemId);
        List<ItemsSpec> itemsSpecs = itemService.queryItemSpecList(itemId);
        ItemsParam itemsParam = itemService.queryItemParamById(itemId);

        ItemInfoVO itemInfoVO =  new ItemInfoVO();
        itemInfoVO.setItem(items);
        itemInfoVO.setItemImgList(itemsImgs);
        itemInfoVO.setItemSpecList(itemsSpecs);
        itemInfoVO.setItemParams(itemsParam);

        return JSONResult.ok(itemInfoVO);
    }

    @ApiOperation(value = "查询商品评价等级", notes = "查询商品评价等级" ,httpMethod = "GET")
    @Transactional(propagation = Propagation.SUPPORTS)
    @GetMapping("/commentLevel")
    public JSONResult commentLevel(@ApiParam(name = "itemId", value = "商品id", required = true)
                               @RequestParam String itemId) {
        if (StringUtils.isBlank(itemId)) {
            return JSONResult.errorMsg("商品ID不能位空");
        }

        CommentLevelCountsVO commentLevelCountsVO = itemService.queryCommentCounts(itemId);

        return JSONResult.ok(commentLevelCountsVO);
    }


    @ApiOperation(value = "查询商品评论", notes = "查询商品评论" ,httpMethod = "GET")
    @Transactional(propagation = Propagation.SUPPORTS)
    @GetMapping("/comments")
    public JSONResult comments(@ApiParam(name = "itemId", value = "商品id", required = true)
                                   @RequestParam String itemId,
                               @ApiParam(name = "level", value = "评价等级")
                               @RequestParam(required = false) Integer level,
                               @ApiParam(name = "page", value = "查询下一页的第几页")
                                   @RequestParam(required = false) Integer page,
                               @ApiParam(name = "pageSize", value = "分页的每一页显示的条数")
                                   @RequestParam(required = false) Integer pageSize) {
        if (StringUtils.isBlank(itemId)) {
            return JSONResult.errorMsg("商品ID不能位空");
        }
        //if (StringUtils.isBlank(level))
        if (page == null) {
            page = 1;
        }
        if (pageSize == null) {
            pageSize = COMMON_PAGE_SIZE;
        }

        PagedGridResult result = itemService.queryPagedComments(itemId, level, page, pageSize);

        return JSONResult.ok(result);
    }


    @ApiOperation(value = "搜索商品列表", notes = "搜索商品列表" ,httpMethod = "GET")
    @Transactional(propagation = Propagation.SUPPORTS)
    @GetMapping("/search")
    public JSONResult search(@ApiParam(name = "keywords", value = "关键字", required = true)
                               @RequestParam String keywords,
                               @ApiParam(name = "sort", value = "排序")
                               @RequestParam(required = false) String sort,
                               @ApiParam(name = "page", value = "查询下一页的第几页")
                               @RequestParam(required = false) Integer page,
                               @ApiParam(name = "pageSize", value = "分页的每一页显示的条数")
                               @RequestParam(required = false) Integer pageSize) {
        if (StringUtils.isBlank(keywords)) {
            return JSONResult.errorMsg("关键字不能位空");
        }

        if (page == null) {
            page = 1;
        }
        if (pageSize == null) {
            pageSize = PAGE_SIZE;
        }

        PagedGridResult result = itemService.searchItems(keywords, sort, page, pageSize);

        return JSONResult.ok(result);
    }

    @ApiOperation(value = "通过分类ID搜索商品列表", notes = "通过分类ID搜索商品列表" ,httpMethod = "GET")
    @Transactional(propagation = Propagation.SUPPORTS)
    @GetMapping("/catItems")
    public JSONResult searchCateItems(@ApiParam(name = "catId", value = "关键字", required = true)
                             @RequestParam Integer catId,
                             @ApiParam(name = "sort", value = "排序")
                             @RequestParam(required = false) String sort,
                             @ApiParam(name = "page", value = "查询下一页的第几页")
                             @RequestParam(required = false) Integer page,
                             @ApiParam(name = "pageSize", value = "分页的每一页显示的条数")
                             @RequestParam(required = false) Integer pageSize) {
        if (catId == null) {
            return JSONResult.errorMsg("分类ID不能位空");
        }

        if (page == null) {
            page = 1;
        }
        if (pageSize == null) {
            pageSize = PAGE_SIZE;
        }

        PagedGridResult result = itemService.searchItemsByThirdCat(catId, sort, page, pageSize);

        return JSONResult.ok(result);
    }

    // 用于用户长时间未登录网站，刷新购物车中的数据（主要是商品价格），类似京东淘宝
    @ApiOperation(value = "根据商品规格ids查找最新的商品数据", notes = "根据商品规格ids查找最新的商品数据", httpMethod = "GET")
    @GetMapping("/refresh")
    public JSONResult refresh(
            @ApiParam(name = "itemSpecIds", value = "拼接的规格ids", required = true, example = "1001,1003,1005")
            @RequestParam String itemSpecIds) {

        if (StringUtils.isBlank(itemSpecIds)) {
            return JSONResult.ok();
        }

        List<ShopcartVO> list = itemService.queryItemsBySpecIds(itemSpecIds);

        return JSONResult.ok(list);
    }

}
