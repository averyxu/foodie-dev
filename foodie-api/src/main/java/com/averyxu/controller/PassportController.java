package com.averyxu.controller;

import com.averyxu.pojo.Users;
import com.averyxu.pojo.bo.UserBO;
import com.averyxu.service.UserService;
import com.averyxu.utils.CookieUtils;
import com.averyxu.utils.JSONResult;
import com.averyxu.utils.JsonUtils;
import com.averyxu.utils.MD5Utils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Api(value = "注册登录", tags = {"用于注册登录的相关接口"})
@RestController
@RequestMapping("passport")
public class PassportController {

    @Autowired
    private UserService userService;

    @ApiOperation(value = "用户名是否存在", notes = "检查用户名是否存在" ,httpMethod = "GET")
    @GetMapping("/usernameIsExist")
    public JSONResult usernameIsExist(@RequestParam String username) {
        if (StringUtils.isBlank(username)) {
            return JSONResult.errorMsg("用户名不能位空！");
        }

        if (userService.queryUsernameIsExist(username)) {
            return JSONResult.errorMsg("用户名已经存在！");
        }

        return JSONResult.ok();
    }

    @ApiOperation(value = "用户注册", notes = "用户注册api" ,httpMethod = "POST")
    @PostMapping("/regist")
    public JSONResult regist(@RequestBody UserBO userBO,
                             HttpServletRequest request,
                             HttpServletResponse response) {
        String username = userBO.getUsername();
        String password = userBO.getPassword();
        String confirmPassword = userBO.getConfirmPassword();

        if (StringUtils.isBlank(username)
                || StringUtils.isBlank(password)
                || StringUtils.isBlank(confirmPassword)) {
            return JSONResult.errorMsg("用户名和密码不能位空！");
        }

        if (userService.queryUsernameIsExist(username)) {
            return JSONResult.errorMsg("用户名已经存在！");
        }

        if (password.length() < 6) {
            return JSONResult.errorMsg("密码长度必须超过6位！");
        }

        if (!password.equals(confirmPassword)) {
            return JSONResult.errorMsg("两次密码不一致！");
        }

        Users result = userService.createUser(userBO);


        //设置为null 没必要缓存到cookie
        result.setPassword(null);
        result.setRealname(null);
        result.setBirthday(null);

        CookieUtils.setCookie(request, response, "user", JsonUtils.objectToJson(result), true);
        return JSONResult.ok();
    }

    @ApiOperation(value = "用户登录", notes = "用户登录api" ,httpMethod = "POST")
    @PostMapping("/login")
    public JSONResult login(@RequestBody UserBO userBO,
                            HttpServletRequest request,
                            HttpServletResponse response) {
        String username = userBO.getUsername();
        String password = userBO.getPassword();

        if (StringUtils.isBlank(username)
                || StringUtils.isBlank(password)) {
            return JSONResult.errorMsg("用户名和密码不能位空！");
        }

        Users result = null;
        try {
            result = userService.queryUserForLogin(username, MD5Utils.getMD5Str(password));
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (result == null) {
            return JSONResult.errorMsg("用户名和密码不正确！");
        }

        //设置为null 没必要缓存到cookie
        result.setPassword(null);
        result.setRealname(null);
        result.setBirthday(null);

        CookieUtils.setCookie(request, response, "user", JsonUtils.objectToJson(result), true);

        //TODO 生成用户token,存入redis会话
        //TODO 同步购物车数据
        return JSONResult.ok(result);
    }


    @ApiOperation(value = "用户退出登录", notes = "用户退出登录api" ,httpMethod = "POST")
    @PostMapping("/logout")
    public JSONResult logout(@RequestParam String userId,
                             HttpServletRequest request,
                             HttpServletResponse response) {
        CookieUtils.deleteCookie(request, response, "user");
        return JSONResult.ok();
    }

}
